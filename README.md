# Welcome to Memeservices

This is a project to host curated, rare memes.
New releases coming soon.
Powered by [Polymer](https://github.com/Polymer/polymer-cli), [Firebase](https://github.com/firebase/firebase-tools), and [Detectify](https://detectify.com)

## Build statuses

### Gitlab Pipeline Status

[![Build Status](https://gitlab.com/ramseymcgrath/MEMESERVICES/badges/master/build.svg)](https://gitlab.com/ramseymcgrath/MEMESERVICES/badges/master/build.svg)

### Travis-CI with Github host

[![Build Status](https://travis-ci.org/ramseymcgrath/MEMESERVICES.svg?branch=master)](https://travis-ci.org/ramseymcgrath/MEMESERVICES)

### Coverity Status

[![Coverity Scan Build Status](https://img.shields.io/coverity/scan/15261.svg)](https://scan.coverity.com/projects/ramseymcgrath-memeservices)

## Local Setup

### Prerequisites

First, install [Polymer CLI](https://github.com/Polymer/polymer-cli) using
[npm](https://www.npmjs.com) (we assume you have pre-installed [node.js](https://nodejs.org)). Use the command `npm install -g polymer-cli`

Second, install [Bower](https://bower.io/) using [npm](https://www.npmjs.com) with `npm install -g bower`

Third, install [Firebase CLI](https://github.com/firebase/firebase-tools) using [npm](https://www.npmjs.com) `npm install -g firebase-tools`

### Build

Once your prereq's are installed, pull in the bower requirements using `bower install`

Now use your favorite editor and change the app as you see fit.

The `polymer build` command builds your Memeservice application for production, using build configuration options provided by the command line or in your `polymer.json` file.

To run locally, use `polymer serve`

## Web Deployment

### Basic Deployment

First login to firebase with `firebase login`

Now deploy! `firebase deploy`

You should now have your memes ready to go

### Kubernetes

There is a sister project to enable kubernetes deployment once the docker image is ready, located here: [MEMESERVICES-Kubernetes](https://gitlab.com/ramseymcgrath/memeservices-kubernetes/)

## API

Work in progress, adding api functionalisty for slack

Contact: me@ramseymcgrath.com, or submit a bug report